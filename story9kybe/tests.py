from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from .forms import *
from django.apps import apps
from .apps import Story9KybeConfig
from django.http import HttpRequest




# Create your tests here.
class Story9Test(TestCase):

    def test_index_url_html (self):
        response = Client().get(reverse("story9kybe:index"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9.html')

    def test_login_url_html(self):
        response = Client().get(reverse("story9kybe:login"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')
    
    def test_template_logout(self):
        response = Client().get(reverse("story9kybe:logout"))
        self.assertEqual(response.status_code,302)

    def test_url_signup(self):
        response = Client().get(reverse("story9kybe:signup"))
        self.assertEqual(response.status_code, 200)

    def test_form_post(self):
        signUp_form = SignUpForm(data={'email':'okdoki@ui.com', 'full_name': 'okdoki'})
        self.assertFalse(signUp_form.is_valid())
        self.assertEqual(signUp_form.cleaned_data['email'],"okdoki@ui.com")
        self.assertEqual(signUp_form.cleaned_data['full_name'],"okdoki") 

    def test_apps(self):
        self.assertEqual(Story9KybeConfig.name, 'story9kybe')
        self.assertEqual(apps.get_app_config('story9kybe').name, 'story9kybe')
    

    def test_selamatDatang(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Selamat Datang",html_response)


