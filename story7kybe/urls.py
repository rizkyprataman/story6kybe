from django.urls import path

from . import views

app_name = 'story7kybe'

urlpatterns = [
    path('', views.home, name='home'),
]
