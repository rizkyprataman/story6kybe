from django.shortcuts import render,redirect
from .models import Member, Activity
from .forms import MemberForm ,ActivityForm

# Create your views here.
def activity(request):
    if request.method == "POST":
        formA = ActivityForm(request.POST)
        formB = MemberForm(request.POST)
        if formA.is_valid():
            formA.save()
            return redirect('main:activity')
        elif formB.is_valid():
            formB.save()
            return redirect('main:activity')
        else:
            return redirect('main:activity')
    else:
        formA = ActivityForm()
        formB = MemberForm()
        activities = Activity.objects.all()
        members = Member.objects.all()
        context = {
            'formA' : formA,
            'formB' : formB,
            'activities' : activities,
            'members' : members
        }
        return render(request, 'main/home.html', context)

def delete(request,pk):
    activity = Activity.objects.get(id=pk)
    activity.delete()
    return redirect('main:activity')

def deleteMember(request,pk):
    member = Member.objects.get(id=pk)
    member.delete()
    return redirect('main:activity')

